# BachelorProject

**Slovak University of Technology in Bratislava**
**FACULTY OF ELECTRICAL ENGINEERING AND INFORMATION TECHNOLOGY**


**Field of study:** 		cybernetics

**Study Programme:** 		Automotive mechatronics

**Author:**			Pavol Kostolanský

**Title:**			Riadenie rýchlosti a polohy robotického vozidla

**Supervisor:**			Ing. Michal Kocúr, PhD.

**Submitted:**			may 2020


**Keywords:** FPGA, Cascade control, PID controller, DC motor, Basys 3, Xilinx Artix-7,  MicroBlaze

Following bachelor thesis analyzes FPGA and its use in the development of dedicated hardware suitable for speed and position control.
Moreover, the thesis describes the principles of FPGA circuits and the flow of hardware development. The subsequent section provides a 
basic explanation of methods used for dynamic system identification as well as a discrete PID control loop mechanism. Additionally, 
work includes hardware design and software platform implementation on FPGA. The application layer of the software platform contains 
cascade control algorithms for direct current motor validated by the system feedback observation and comparison with a direct current
motor model simulation.




