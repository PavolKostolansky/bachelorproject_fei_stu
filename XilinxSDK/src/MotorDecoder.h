/**
 * @file MotorDecoder.h
 * @brief S�bor obsahuje API, ktor� pon�ka MotorDecoder ovl�da� a taktie� �trukt�ru MotorDecoder.
 *
 */

#ifndef SRC_MOTORDECODER_H_
#define SRC_MOTORDECODER_H_

/****************************** Include Files ********************************/

#include "xil_types.h"

/**************************** Macro Definitions ********************************/


/***************************** Type Definitions ********************************/

/** @struct MotorDecoder
 *  @brief T�to �trukt�ra sl��i na uchovanie parametrov perif�rie Decoder
 *  @var MotorDecoder::base_addr
 *  Premenn� 'base_addr' uchov�va adresu perif�rie pre AXI protokol
 *  @var MotorDecoder::clkFreqHz
 *  Premenn� 'clkFreqHz' uchov�va frekvenciu, na ktor� je pripojen� perif�ria Decoder
 */
typedef struct MotorDecoder {
   u32 base_addr;
   u32 clkFreqHz;
} MotorDecoder;

/*************************** Function Prototypes *******************************/

void MotorDecoder_init();

void MotorDecoder_updateMeasuredValues();


/***************************** Global Variables ******************************/


#endif /* SRC_MOTORDECODER_H_ */
