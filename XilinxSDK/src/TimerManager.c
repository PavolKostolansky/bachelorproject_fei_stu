/**
 * @file TimerManager.c
 * @brief TimerManager sl��i ako rozhranie pre interakciu s �asova�om.
 *
 *## Funkcionalita TimerManager
 *Hlavn� �loha TimerManager je inicializ�cia �asova�a pre Scheduler, ktor� riadi volanie
 *funkci�. �asova� je nastaven� pod�a peri�dy definovanej v PlatformConfig a pon�ka API
 *na zmenu peri�dy a na zresetovanie �asova�a.
 */

/****************************** Include Files ********************************/
#include "TimerManager.h"
#include "PlatformConfig.h"
#include "Scheduler.h"

/*************************** Function Prototypes *******************************/



/*************************** Function Definitions ****************************/

 /****************************************************************************/
 /**
 *
 * \fn void TimerManager_initTimer()
 *
 * Funkcia, ktor� inicializuje �asova� platformy, prirad� ho k �trukt�re
 * XTmrCtr a taktie� prirad� Scheduler_timerCounterHandler funkciu ako "handler"
 * preru�enia. Je volan� pri inicializ�ci� platformy.
 *
 *
 * @return	void
 *
 *
 ****************************************************************************/
void TimerManager_initTimer(){

		XTmrCtr_Initialize(&TimerCounterInst, TMRCTR_DEVICE_ID);

		XTmrCtr_SelfTest(&TimerCounterInst, TIMER_CNTR_0);

		XTmrCtr_SetHandler(&TimerCounterInst, Scheduler_timerCounterHandler,
						   &TimerCounterInst);

		XTmrCtr_SetOptions(&TimerCounterInst, TIMER_CNTR_0,
					XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION);

		XTmrCtr_SetResetValue(&TimerCounterInst, TIMER_CNTR_0, RESET_VALUE);

		XTmrCtr_Start(&TimerCounterInst, TIMER_CNTR_0);

}


/****************************************************************************/
/**
*
* \fn void TimerManager_changeTimerPeriod(u8 ID,u8 *data)
*
* Funkcia sl��i na zmenu peri�dy �asova�a platformy.
*
* @param
*    [in] ID:  ID API, z ktorej prich�dzaj� d�ta.
* @param
*    [in] data: pointer na typ u8.
*
* @return	void
*
*
****************************************************************************/
void TimerManager_changeTimerPeriod(u8 ID,u8 *data){

	u32 resetCount=0;

	resetCount = 0xFFFFFFFF - ((data[0])*XPAR_TMRCTR_0_CLOCK_FREQ_HZ/1000);

	XTmrCtr_SetResetValue(&TimerCounterInst,TIMER_CNTR_0,resetCount);
	XTmrCtr_Reset(&TimerCounterInst,TIMER_CNTR_0);
}

/****************************************************************************/
/**
*
* \fn void TimerManager_resetTimer()
*
* Funkcia sl��i na zresetovanie �asova�a.
*
*
* @return	void
*
*
****************************************************************************/
void TimerManager_resetTimer(){
	XTmrCtr_Reset(&TimerCounterInst,TIMER_CNTR_0);
}





