/**
 * @file Scheduler.h
 * @brief S�bor obsahuje API, ktor� pon�ka Scheduler.
 *
 */

#ifndef SRC_SCHEDULER_H_
#define SRC_SCHEDULER_H_

/***************************** Include Files *******************************/
#include <xil_types.h>

/*************************** Function Prototypes *******************************/
void Scheduler_run()__attribute__((optimize(0)));

void Scheduler_timerCounterHandler(void *CallBackRef, u8 TmrCtrNumber);

#endif /* SRC_SCHEDULER_H_ */
