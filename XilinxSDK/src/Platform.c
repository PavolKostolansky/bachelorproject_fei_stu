/**
 * @file Platform.c
 * @brief Platform sl��i na inicializ�ciu syst�mu.
 *
 *## Funkcionalita Platform
 *Hlavn� �loha je inicializova� pri spusten� v�etky perif�rie platformy.
 */

/****************************** Include Files ********************************/
#include "Platform.h"
#include "xil_cache.h"
#include "InterruptManager.h"
#include "TimerManager.h"
#include "MotorFeedback.h"
#include "MotorDecoder.h"
#include "PmodDHB1.h"
#include "UART.h"


#ifdef STDOUT_IS_16550
 #include "xuartns550_l.h"

 #define UART_BAUD 9600
#endif


/*************************** Function Definitions ****************************/

/****************************************************************************/
/**
*
* \fn static void Platform_enable_caches()
*
* Funkcia sl��i vytvorenie regi�nov cache pam�te a jej povolenie.
*
*
* @return	void
*
*
****************************************************************************/
static void Platform_enable_caches(){

#ifdef __PPC__
    Xil_ICacheEnableRegion(CACHEABLE_REGION_MASK);
    Xil_DCacheEnableRegion(CACHEABLE_REGION_MASK);
#elif __MICROBLAZE__
#ifdef XPAR_MICROBLAZE_USE_ICACHE
    Xil_ICacheEnable();
#endif
#ifdef XPAR_MICROBLAZE_USE_DCACHE
    Xil_DCacheEnable();
#endif
#endif
}

/****************************************************************************/
/**
*
* \fn static void Platform_disable_caches()
*
* Funkcia sl��i vy�istenie a zak�zanie cache pam�te
*
*
* @return	void
*
*
****************************************************************************/
static void Platform_disable_caches(){

#ifdef __MICROBLAZE__
#ifdef XPAR_MICROBLAZE_USE_DCACHE
    Xil_DCacheDisable();
#endif
#ifdef XPAR_MICROBLAZE_USE_ICACHE
    Xil_ICacheDisable();
#endif
#endif
}

/****************************************************************************/
/**
*
* \fn static void Platform_UARTLITE_init()
*
* Funkcia sl��i na inicializ�ciu BAUD rate pre perif�riu UARTLITE a vol� taktie�
* funkciu na inicializ�ciu handlerov preru�en�.
*
*
* @return	void
*
*
****************************************************************************/
static void Platform_UARTLITE_init(){

#ifdef STDOUT_IS_16550
    XUartNs550_SetBaud(STDOUT_BASEADDR, XPAR_XUARTNS550_CLOCK_HZ, UART_BAUD);
    XUartNs550_SetLineControlReg(STDOUT_BASEADDR, XUN_LCR_8_DATA_BITS);
#endif
    /* Bootrom/BSP configures PS7/PSU UART to 115200 bps */

    UART_init();
}


/****************************************************************************/
/**
*
* \fn void Platform_init()
*
* Funkcia sl��i na inicializ�ciu v�etk�ch perif�ri� a na povolenie preru�en�
* v syst�me. Je volan� funkciou main pri spusten� syst�mu.
*
*
* @return	void
*
*
****************************************************************************/
void Platform_init(){

	Platform_enable_caches();

    //inicializ�cia UARTLITE
	Platform_UARTLITE_init();

    //inicializ�cia �asova�a
	TimerManager_initTimer();

    //inicializ�cia PmodDHB1
    PmodDHB1_init();

    //inicializ�cia MotorFeedback
    MotorFeedback_init();

    //inicializ�cia MotorDecodera
    MotorDecoder_init();

    //inicializ�cia preru�en�
    InterruptManager_setupInterrupts();

    //povolenie preru�en� v platforme
    InterruptManager_enableInterrupts();
}

/****************************************************************************/
/**
*
* \fn void Platform_cleanup()
*
* Funkcia sl��i na vy�istenie a zak�zanie cache pam�te po dokon�en� behu platformy.
*
*
* @return	void
*
*
****************************************************************************/
void Platform_cleanup()
{
	Platform_disable_caches();
}
