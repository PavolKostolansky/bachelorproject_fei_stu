/**
 * @file Platform.h
 * @brief S�bor obsahuje API, ktor� pon�ka Platform.
 *
 */

#ifndef __PLATFORM_H_
#define __PLATFORM_H_


/****************************** Include Files ********************************/
#include "PlatformConfig.h"


/*************************** Function Prototypes *******************************/

void Platform_init();

void Platform_cleanup();

#endif
