/**
 * @file COMM.h
 * @brief S�bor obsahuje API, ktor� pon�ka COMM vrstva.
 *
 */

#ifndef SRC_COMM_H_
#define SRC_COMM_H_

/****************************** Include Files ********************************/
#include <xil_types.h>
#include "COMMDefs.h"

/**************************** Macro Definitions ********************************/




/*************************** Function Prototypes *******************************/

void COMM_Receive(u8 ID,u8 *data);

void COMM_Transmit(u8 ID,u8 *data);

/*! @def COMM_receive(ID,data)
    @brief Makro zadefinovan� kv�li sp�tnej kompatibilite.
*/
#define COMM_receive(ID,data) COMM_Receive(ID,data)

/*! @def COMM_transmit(ID,data)
    @brief Makro zadefinovan� kv�li sp�tnej kompatibilite.
*/
#define COMM_transmit(ID,data) COMM_Transmit(ID,data)


/***************************** Global Variables ******************************/








#endif /* SRC_COMM_H_ */
