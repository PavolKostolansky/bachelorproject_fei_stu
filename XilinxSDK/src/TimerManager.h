/**
 * @file TimerManager.h
 * @brief S�bor obsahuje API, ktor� pon�ka TimerManager.
 *
 */

#ifndef SRC_TIMERMANAGER_H_
#define SRC_TIMERMANAGER_H_

/***************************** Include Files *******************************/
#include "xtmrctr.h"

/************************** Function Prototypes ******************************/
void TimerManager_initTimer();

void TimerManager_changeTimerPeriod(u8 ID,u8 *data);

void TimerManager_resetTimer();

/***************************** Global Variables ******************************/
XTmrCtr TimerCounterInst;




#endif /* SRC_TIMERMANAGER_H_ */
