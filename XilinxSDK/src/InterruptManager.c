/**
 * @file InterruptManager.c
 * @brief InterruptManager inicializuje a riadi preru�enia v platforme.
 *
 *## Funkcionalita InterruptManager
 *InterruptManager pon�ka API na povolenie a zak�zanie preru�en� v platforme a
 *FastHandler pre preru�enie �asova�a.
 */

/***************************** Include Files *********************************/
#include "TimerManager.h"
#include "InterruptManager.h"
#include "xintc.h"
#include "xil_exception.h"
#include "PlatformConfig.h"
#include "UART.h"

/************************** Function Prototypes ******************************/

static void InterruptManager_TmrCtr_FastHandler(void)  \
__attribute__ ((fast_interrupt));


/***************************** Global Variables ******************************/

 XIntc InterruptController;      /* The instance of the Interrupt Controller */


/************************** Function Definitions ***************************/

 /****************************************************************************/
 /**
 *
 * \fn void InterruptManager_disableInterrupts()
 *
 * Funkcia, ktor� zak�e preru�enia v celej platforme. Vo funkci� s� priraden� adresy
 * existuj�cich preru�en� v syst�me.
 *
 *
 * @return	void
 *
 *
 ****************************************************************************/
 void InterruptManager_disableInterrupts(){

	 	 XIntc_Disable(&InterruptController, TMRCTR_INTERRUPT_ID);
		 XIntc_Disable(&InterruptController, UARTLITE_INT_IRQ_ID);


 }

 /****************************************************************************/
 /**
 *
 * \fn void InterruptManager_enableInterrupts()
 *
 * Funkcia, ktor� povol� preru�enia v celej platforme. Vo funkci� s� priraden� adresy
 * existuj�cich preru�en� v syst�me.
 *
 *
 * @return	void
 *
 *
 ****************************************************************************/
 void InterruptManager_enableInterrupts(){

	 	 XIntc_Enable(&InterruptController, TMRCTR_INTERRUPT_ID);
		 XIntc_Enable(&InterruptController, UARTLITE_INT_IRQ_ID);

 }

 /****************************************************************************/
 /**
 *
 * \fn void InterruptManager_setupInterrupts()
 *
 * Funkcia, ktor� inicializuje v�etky preru�enia v syst�me, prirad� ich k �trukt�re
 * XIntc a taktie� prirad� FastHandler �asova�a a handler UART preru�en�.
 * Je volan� pri inicializ�ci� platformy.
 *
 *
 * @return	void
 *
 *
 ****************************************************************************/
void InterruptManager_setupInterrupts(){


	XIntc_Initialize(&InterruptController, INTC_DEVICE_ID);

	XIntc_ConnectFastHandler(&InterruptController, TMRCTR_INTERRUPT_ID,
				(XFastInterruptHandler)InterruptManager_TmrCtr_FastHandler);

	XIntc_Connect(&InterruptController, UARTLITE_INT_IRQ_ID,
			(XInterruptHandler)XUartLite_InterruptHandler,
			(void *)&UartLite);

	XIntc_Start(&InterruptController, XIN_REAL_MODE);

	Xil_ExceptionInit();

	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
					(Xil_ExceptionHandler)
					XIntc_InterruptHandler,
					&InterruptController);

	Xil_ExceptionEnable();

}

/****************************************************************************/
/**
*
* \fn static void InterruptManager_TmrCtr_FastHandler()
*
* Funkcia FastHandlera pre �asova�.
*
* @return	void
*
*
****************************************************************************/
static void InterruptManager_TmrCtr_FastHandler(void)
{

	XTmrCtr_InterruptHandler(&TimerCounterInst);
}
