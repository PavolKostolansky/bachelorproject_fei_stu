/**
 * @file InterruptManager.h
 * @brief S�bor obsahuje API, ktor� pon�ka InterruptManager.
 *
 */

#ifndef SRC_INTERRUPTMANAGER_H_
#define SRC_INTERRUPTMANAGER_H_

/**************************** Macro Definitions ********************************/




/*************************** Function Prototypes *******************************/

void InterruptManager_setupInterrupts();

void InterruptManager_disableInterrupts();

void InterruptManager_enableInterrupts();



/***************************** Global Variables ******************************/





#endif /* SRC_INTERRUPTMANAGER_H_ */
