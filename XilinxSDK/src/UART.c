/**
 * @file UART.c
 * @brief UART ovl�da� sl��i ako rozhranie pre interakciu s perif�riou UARTLITE.
 *
 *## Funkcionalita UART
 *Hlavn� �loha UART ovl�da�a je posielanie a prij�manie d�t cez UART protokol.
 *Na za�iatku pon�ka funkciu, ktor� inicializuje "handler" funkcie pre preru�enie,
 *ktor� je sp�soben� prijat�m d�t, alebo odoslan�m v�etk�ch d�t ako indik�cia,
 *�e UART TX buffer je pr�zdny.
 */

/***************************** Include Files *******************************/
#include "UART.h"
#include "InterruptManager.h"
#include "COMM.h"
#include "PlatformConfig.h"
#include "Types.h"

/**************************** Macro Definitions ********************************/

#define XUL_CONTROL_REG_OFFSET		12

/************************** Function Prototypes ******************************/

void UART_sendHandler(void *CallBackRef, unsigned int EventData);

void UART_recvHandler(void *CallBackRef, unsigned int EventData);


/***************************** Global Variables ******************************/


XUartLite_Config *UartLite_Cfg;

static u8 asciiArr[]="0123456789ABCDEF";


/************************** Function Definitions ***************************/

/****************************************************************************/
/**
*
* \fn void UART_init()
*
* Funkcia, ktor� inicializuje UART perif�riu. Taktie� prirad� jej parametre k �trukt�re
* XUartLite_Config (implementovan� v BSP - basic software package) a "handler" funkcie
* pre RX a TX preru�enia. Je volan� pri inicializ�ci� platformy.
*
*
* @return	void
*
*
****************************************************************************/
void UART_init(){

		XUartLite_Initialize(&UartLite, UARTLITE_DEVICE_ID);

		XUartLite_SelfTest(&UartLite);

		XUartLite_SetSendHandler(&UartLite, UART_sendHandler, &UartLite);
		XUartLite_SetRecvHandler(&UartLite, UART_recvHandler, &UartLite);


		XUartLite_EnableInterrupt(&UartLite);
}


/****************************************************************************/
/**
*
* \fn void UART_write(u8 ID , u8 *data)
*
* Funkcia sl��i na odosielanie d�t cez UART perif�riu. Pod�a ID s� d�ta p�san� bu�
* ako jeden ASCII znak alebo pre lep�ie pochopenie ako HEX hodnota.
*
* @param
*    [in] ID:  ID volania funkcie.
* @param
*    [in] data: pointer na typ u8.
*
*
* @return	void
*
*
****************************************************************************/
void UART_write(u8 ID , u8 *data){

	u8 byte[6]={0};


	switch(ID)
	{
	case UART_WRITE_NUMBER :

		byte[0]=' ';
		byte[1]='0';
		byte[2]='x';
		byte[3]=asciiArr[(((*data)>>4) & 0b00001111)];
		byte[4]=asciiArr[((*data) & 0b00001111)];
		byte[5]=' ';

		XUartLite_Send(&UartLite, byte, 6);
		break;
	case UART_WRITE_CHAR :
		XUartLite_Send(&UartLite, data, 1);
		break;
	}


}


/****************************************************************************/
/**
*
* \fn void UART_sendHandler(void *CallBackRef, unsigned int EventData)
*
* Funkcia sl��i ako "handler" preru�enia pri odoslan� v�etk�ch d�t z TX buffera.
*
* @param
*    [in] CallBackRef:  Pointer na typ void.
* @param
*    [in] EventData:  Premenn� typu int, charakterizuj�ca po�et prijat�ch d�t.
*
*
* @return	void
*
*
****************************************************************************/
void UART_sendHandler(void *CallBackRef, unsigned int EventData){



}

/****************************************************************************/
/**
*
* \fn void UART_recvHandler(void *CallBackRef, unsigned int EventData)
*
* Funkcia sl��i ako "handler" preru�enia pri prijat� d�t cez UART perif�riu.
* N�sledn� s� d�ta poslan� cez komunika�n� vrstvu do aplika�nej, kde s�
* aktualizovan�.
*
* @param
*    [in] CallBackRef:  Pointer na typ void.
* @param
*    [in] EventData:  Premenn� typu int, charakterizuj�ca po�et prijat�ch d�t.
*
*
* @return	void
*
*
****************************************************************************/
void UART_recvHandler(void *CallBackRef, unsigned int EventData){

	u8 receivedData;

	XUartLite_Recv(&UartLite, &receivedData, 1);

	// if the dataflow will be bigger we can use buffer
    //bufferWrite(&rx_buffer, recievedData);

	COMM_receive(UART_RECEIVED_DATA, &receivedData );
}
