/**
 * @file PlatformConfig.h
 * @brief S�bor obsahuje makr� prepojen� s "hardware definition file",
 * ktor�ho makr� s� ulo�en� v xparameters.h. Taktie� obsahuje z�kladn� parametre
 * pre r�zne perif�rie syst�mu ako s� PmodDHB1 alebo Timer.
 *
 */

#ifndef SRC_PLATFORMCONFIG_H_
#define SRC_PLATFORMCONFIG_H_

/***************************** Include Files *********************************/

#include "xparameters.h"

/************************** Constant Definitions *****************************/

#define PMOD_DBH_1              XPAR_PMODGPIO_0_AXI_LITE_GPIO_BASEADDR
#define UARTLITE_DEVICE_ID      XPAR_UARTLITE_0_DEVICE_ID
#define INTC_DEVICE_ID          XPAR_INTC_0_DEVICE_ID
#define UARTLITE_INT_IRQ_ID     XPAR_INTC_0_UARTLITE_0_VEC_ID
#define GPIO_REG_BASEADDR		0x40000000
#define GPIO_DEVICE_ID			XPAR_GPIO_0_DEVICE_ID
#define TIMER_INT_IRQ_ID		XPAR_AXI_INTC_0_FIT_TIMER_0_INTERRUPT_INTR
#define TMRCTR_DEVICE_ID		XPAR_TMRCTR_0_DEVICE_ID
#define INTC_DEVICE_ID			XPAR_INTC_0_DEVICE_ID
#define TMRCTR_INTERRUPT_ID		XPAR_INTC_0_TMRCTR_0_VEC_ID
#define GPIO_BASEADDR    		XPAR_PMODDHB1_0_AXI_LITE_GPIO_BASEADDR
#define PWM_BASEADDR      		XPAR_PMODDHB1_0_PWM_AXI_BASEADDR
#define MOTOR_FB_BASEADDR 		XPAR_PMODDHB1_0_MOTOR_FB_AXI_BASEADDR
#define MOTOR_DECODER_BASEADDR 	XPAR_SLV_TO_AXI_0_S00_AXI_BASEADDR

#ifdef __MICROBLAZE__
#define CLK_FREQ XPAR_CPU_M_AXI_DP_FREQ_HZ
#else
#define CLK_FREQ 100000000
#endif


#define TIMER_CNTR_0	 0

#define TIMER_PERIOD 		1

#define RESET_VALUE	 (0xFFFFFFFF - \
					 (TIMER_PERIOD*XPAR_TMRCTR_0_CLOCK_FREQ_HZ/1000))

#define PWM_PER              1
#define SENSOR_EDGES_PER_REV 8
#define GEARBOX_RATIO        75
#define MAX_VOLTAGE			 9

#endif /* SRC_PLATFORMCONFIG_H_ */
