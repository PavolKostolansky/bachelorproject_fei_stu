/**
 * @file AppHandle.c
 * @brief Aplika�n� vrstva platformy vytvorenej pre MicroBlaze softcore procesor.
 *
 *## Funkcionalita vrstvy AppHandle
 *V aplika�nej vrstve s� vytvoren� jednoduch� firmv�rov� aplik�cie, ktor� m��u by�
 *umiestnen� do Schedulera alebo volan� z ni���ch vrstiev platformy ako funkcia AppHandle_receive
 *ktor� je volan� z UART ovl�da�a a ovl�da�a perif�ri� dek�dera a PmodDHB1.
 */

/****************************** Include Files ********************************/

#include "AppHandle.h"
#include "COMM.h"
#include "PmodDHB1.h"
#include "PlatformConfig.h"

/**************************** Macro Definitions ********************************/

#define P (float)1.4
#define I (float)0.1
#define D (float)0.1

#define T   (float)0.01
#define T_I (float)(P/I)
#define T_D (float)(D/P)

#define POS_Q0  (float)(P*(1+(T_D/T)+(T/T_I)))
#define POS_Q1  (float)(-P*(1+2*(T_D/T)))
#define POS_Q2  (float)(P*(T_D/T))


#define SPEED_Q0 (float)0.1863
#define SPEED_Q1 (float)-0.1689

#define INF 0xFF

#define FINAL_POSITION (s32)1000

#ifdef MEASUREMENT
#define MEASUREMENT_TIME 3
#endif


/*************************** Function Prototypes *******************************/

static void AppHandle_PSD_regulatorPosition();

static void AppHandle_PS_regulatorSpeed();


/***************************** Global Variables ******************************/

u8 direction;

s32 inputPosition=FINAL_POSITION,
	actualPosition;

float actualSpeedRPM;
float inputValueSpeedRPM;

#ifdef MEASUREMENT
float speedArr[MEASUREMENT_TIME*100]={0};
float voltageArr[MEASUREMENT_TIME*100]={0};
float positionArr[MEASUREMENT_TIME*100]={0};
float desiredSpeedArr[MEASUREMENT_TIME*100]={0};

bool measurementDone=false;
#endif

bool init = false;

/*************************** Function Definitions ****************************/

/****************************************************************************/
/**
*
* \fn void AppHandle_receive(u8 ID,u8 *data)
*
*
* Funkcia, ktor� sl��i na aktualiz�ciu d�t v aplika�nej vrstve, ktor� prech�dzaj� z ovl�da�ov
* a spodn�ch vrstiev platformy cez komunika�n� vrstvu a� do aplika�nej.
*
* @param
*    [in] ID:  ID API, z ktorej prich�dzaj� d�ta.
* @param
*    [in] data: pointer na typ u8.
*
* @return	void
*
*
****************************************************************************/
void AppHandle_receive(u8 ID,u8 *data){

	u16 timeDiff=0;

	u32 edgesCount = 0;

	#ifdef MEASUREMENT
	static u32 count = 0;
	#endif

	switch(ID){
	case UART_RECEIVED_DATA:



		break;

	case MOTOR_FEEDBACK_EDGES_COUNT:

		edgesCount = (((u16)(data[1]<<8)) | data[0]);

		break;

	case MOTOR_DECODER_TIME_DIFF:

		timeDiff =(((u16)(data[1]<<8)) | data[0]);

		actualSpeedRPM = (float)( 60*1000000 / \
				(float)( timeDiff*SENSOR_EDGES_PER_REV*GEARBOX_RATIO ));



		break;
	case MOTOR_DECODER_DIR:

		direction = data[0];

		break;
	case MOTOR_DECODER_EDGES_COUNT:

		actualPosition =(((s32)(data[3]<<24)) | \
						(data[2]<<16) | (data[1]<<8) | data[0]);

		AppHandle_PSD_regulatorPosition();

		break;
	}
}

/****************************************************************************/
/**
*
* \fn static void AppHandle_PSD_regulatorPosition()
*
*
* Implement�cia PSD regul�tora polohy. Zapisuje ak�n� z�sah do �iadanej r�chlosti,
* ktor� pou��va regul�tor r�chlosti. Implementovan� anti-windup a satur�cia ak�n�ho
* z�sahu. Vol� funkciu regul�tora r�chlosti.
*
*
* @return	void
*
*
****************************************************************************/
static void AppHandle_PSD_regulatorPosition(){

	#ifdef MEASUREMENT
	static u32 count=0;
	#endif

	static float e_previous_0 = 0,
				 e_previous_1 = 0,
				 u_previous = 0;

	static s32 e;

	float p0, p1, p2, s1, s2, u;


	e = inputPosition - actualPosition;

	p0 = (float)POS_Q0 * (float) e;
	p1 = (float)POS_Q1 * e_previous_0;
	p2 = (float)POS_Q2 * e_previous_1;

	s1 = p0 + p1 + p2 ;
	s2 = u_previous;

	u = s1 + s2;

	u_previous = u;

	if(u < -90) s1 = 0;
	if(u > 90)  s1 = 0;

	u = s1 + s2;

	if(u < -90) u = -90;
	if(u > 90)  u = 90;

	inputValueSpeedRPM = u;


	e_previous_1 = e_previous_0;
	e_previous_0 = e;


	#ifdef MEASUREMENT
	if(count<(MEASUREMENT_TIME*100)){
	desiredSpeedArr[count]=u;
	count ++;
	}
	#endif

	AppHandle_PS_regulatorSpeed();

}

/****************************************************************************/
/**
*
* \fn static void AppHandle_PSD_regulatorSpeed()
*
*
* Implement�cia PSD regul�tora r�chlosti. Posiela ak�n� z�sah cez komunika�n� vrstvu
* do PmodDHB1 ovl�da�a, kde je ak�n� z�sah prepo��tan� na PWM.
*
*
* @return	void
*
*
****************************************************************************/
static void AppHandle_PS_regulatorSpeed(){

	#ifdef MEASUREMENT
	static u32 count=0;
	#endif

	static float e_previous = 0,
				 u_previous = 0;

	static u8 dir=0;

	float e,p0,p1,s1,u;

	u8 *arrayByte ;


	if(init){
		if(direction) actualSpeedRPM = -actualSpeedRPM;
	}
	else init=true;

	if(actualSpeedRPM>INF) e = inputValueSpeedRPM;
	else e = inputValueSpeedRPM - actualSpeedRPM;

	p0 = SPEED_Q0*e;

	p1 = SPEED_Q1*e_previous;

	s1 = p0 + p1;

	u = s1 + u_previous;


	if(u > 9) u = 9;
	if(u < -9) u = -9;

	u_previous = u;

	#ifdef MEASUREMENT
	if(count<(MEASUREMENT_TIME*100)){
	speedArr[count] = actualSpeedRPM;
	voltageArr[count]=u;
	count ++;
	}
	#endif


	e_previous = e;

	if(u < 0){
		u = -u;
		dir=1;
	}
	else
		dir=0;

	arrayByte = (u8*) (&u);

	COMM_transmit(PMOD_DHB1_SET_DIR_M0,&dir);
	COMM_transmit(PMOD_DHB1_SET_VOLTAGE_M0,arrayByte);

}


/****************************************************************************/
/**
*
* \fn void AppHandle_sendUART()
*
*
* Funkcia, ktor� sl��i na posielanie d�t smerom do UART ovl�da�a.
*
*
* @return	void
*
*
****************************************************************************/
void AppHandle_sendUART(){

	#ifdef MEASUREMENT
	static u32 i=0;

	if (measurementDone) {

		if (i < MEASUREMENT_TIME*100) {

			u8 *arrayByte;

			arrayByte = (u8*) (&positionArr[i]);

			COMM_transmit(UART_WRITE_CHAR, arrayByte);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 1);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 2);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 3);

			arrayByte = (u8*) (&voltageArr[i]);

			COMM_transmit(UART_WRITE_CHAR, arrayByte);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 1);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 2);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 3);

			i++;
		}
		else if (i<MEASUREMENT_TIME*100*2) {

			u8 *arrayByte;

			arrayByte = (u8*) (&speedArr[(i-(MEASUREMENT_TIME*100))]);

			COMM_transmit(UART_WRITE_CHAR, arrayByte);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 1);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 2);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 3);

			arrayByte = (u8*) (&desiredSpeedArr[(i-(MEASUREMENT_TIME*100))]);

			COMM_transmit(UART_WRITE_CHAR, arrayByte);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 1);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 2);
			COMM_transmit(UART_WRITE_CHAR, arrayByte + 3);

			i++;
		}
		else{

		}
	}
	#endif
}
