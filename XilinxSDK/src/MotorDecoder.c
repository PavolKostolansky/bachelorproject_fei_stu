/**
 * @file MotorDecoder.c
 * @brief MotorDecoder sl��i ako ovl�da� pre perif�riu Decoder.
 *
 *## Funkcionalita MotorDecoder
 *Hlavn� �loha ovl�da�a MotorDecoder je aktualizova� �as medzi n�be�n�mi hranami prich�dzaj�cimi
 *z perif�rie Decoder a posiela� aktu�lne inform�cie do aplika�nej vrstvy.
 */


/****************************** Include Files ********************************/

#include "MotorDecoder.h"
#include "PlatformConfig.h"
#include "COMM.h"
#include "xil_io.h"

/**************************** Macro Definitions ********************************/

#define MOTORDECODER_TIME_DIFF_OFFSET	0x00
#define MOTORDECODER_DIR_OFFSET   		0x04
#define MOTORDECODER_POS_COUNT_OFFSET	0x08



/***************************** Global Variables ******************************/

MotorDecoder motorDecoder;


/*************************** Function Prototypes *******************************/

static void MotorDecoder_getTime(u16 *timeDiff);

static void MotorDecoder_getImpulses(s32 *impulses);

static void MotorDecoder_getDirection(u8 *direction);


/*************************** Function Definitions ****************************/

/****************************************************************************/
/**
*
* \fn void MotorDecoder_init()
*
* Funkcia sl��i na inicializ�ciu �trukt�ry MotorDecoder spr�vnymi parametrami perif�rie Decodera.
* Je volan� pri inicializ�ci� platformy.
*
*
* @return	void
*
*
****************************************************************************/
void MotorDecoder_init(){

	motorDecoder.base_addr = MOTOR_DECODER_BASEADDR;
	motorDecoder.clkFreqHz = CLK_FREQ;
}

/****************************************************************************/
/**
*
* \fn void MotorDecoder_updateMeasuredValues()
*
* Funkcia sl��i na aktualiz�ciu meran�ch veli��n z perif�rie enk�dera
* v aplika�nej vrstve. Je volan� v Scheduleri s peri�dou 10 ms.
*
*
* @return	void
*
*
****************************************************************************/
void MotorDecoder_updateMeasuredValues(){

	u16 timeDiff = 0;

	u8 byteArr[4] = {0},
	   direction = 0;

	s32 impulses = 0;


	MotorDecoder_getTime(&timeDiff);
	byteArr[0] = (u8) (timeDiff);
	byteArr[1] = (u8) (timeDiff>>8);
	COMM_receive(MOTOR_DECODER_TIME_DIFF, byteArr);


	MotorDecoder_getDirection(&direction);
	byteArr[0] = (u8) (direction);
	COMM_receive(MOTOR_DECODER_DIR, byteArr);


	MotorDecoder_getImpulses(&impulses);
	byteArr[0] = (u8) (impulses);
	byteArr[1] = (u8) (impulses>>8);
	byteArr[2] = (u8) (impulses>>16);
	byteArr[3] = (u8) (impulses>>24);
	COMM_receive(MOTOR_DECODER_EDGES_COUNT, byteArr);

}

/****************************************************************************/
/**
*
* \fn static void MotorDecoder_getTime(u16 *timeDiff)
*
* Funkcia sl��i na ��tanie hodnoty �asov�ho rozdielu medzi n�be�n�mi hranami z enk�dera
* v us z registra perif�rie dek�dera.
*
** @param
*    [in] timeDiff:  Pointer na typ u16.
*
* @return	void
*
*
****************************************************************************/
static void MotorDecoder_getTime(u16 *timeDiff){

	*timeDiff = Xil_In16(motorDecoder.base_addr + MOTORDECODER_TIME_DIFF_OFFSET);

}

/****************************************************************************/
/**
*
* \fn static void MotorDecoder_getImpulses(s32 *impulses)
*
* Funkcia sl��i na ��tanie po�tu impulzov z registra perif�rie dek�dera.
*
** @param
*    [in] impulses:  Pointer na typ sint32.
*
* @return	void
*
*
****************************************************************************/
static void MotorDecoder_getImpulses(s32 *impulses){

	*impulses = (s32)Xil_In32(motorDecoder.base_addr + \
				MOTORDECODER_POS_COUNT_OFFSET);

}

/****************************************************************************/
/**
*
* \fn static void MotorDecoder_getDirection(u8 *direction)
*
* Funkcia sl��i na ��tanie hodnoty smeru z registra perif�rie dek�dera.
*
** @param
*    [in] direction:  Pointer na typ u8.
*
* @return	void
*
*
****************************************************************************/
static void MotorDecoder_getDirection(u8 *direction){

	*direction = (u8)Xil_In32(motorDecoder.base_addr + MOTORDECODER_DIR_OFFSET);

}
