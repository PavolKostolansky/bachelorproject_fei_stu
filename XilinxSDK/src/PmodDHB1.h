/**
 * @file PmodDHB1.h
 * @brief S�bor obsahuje API, ktor� pon�ka PmodDHB1 rozhranie a taktie� �trukt�ru PmodDHB1.
 *
 */

#ifndef PmodDHB1_H
#define PmodDHB1_H

/****************************** Include Files ********************************/

#include "xil_types.h"


/**************************** Type Definitions *****************************/

/** @struct PmodDHB1
 *  @brief T�to �trukt�ra sl��i na uchovanie parametrov perif�rie PmodDHB1 - PWM.
 *  @var PmodDHB1::GPIO_addr
 *  Premenn� 'GPIO_addr' uchov�va adresu v�stupov pre AXI protokol.
 *  @var PmodDHB1::PWM_addr
 *  Premenn� 'PWM_addr' uchov�va adresu PWM "offsetu" v perif�ri� PmodDHB1 pre AXI protokol.
 *  @var PmodDHB1::PWM_per
 *  Premenn� 'PWM_per' uchov�va peri�du PWM sign�lu.
 */
typedef struct PmodDHB1 {
   u32 GPIO_addr;
   u32 PWM_addr;
   u32 PWM_per;
} PmodDHB1;


/*************************** Function Prototypes *******************************/
void PmodDHB1_init();

void PmodDHB1_motorSetVoltage(u8 ID,u8 *data);

void DHB1_setDir(u8 ID,u8 *data);


#endif // PmodDHB1_H
