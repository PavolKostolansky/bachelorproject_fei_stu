/**
 * @file Main.c
 * @brief Main ako hlavn� riadiaca funkcia inicializ�cie, behu a v�sledn�ho "clean-up".
 *
 *## Funkcionalita Main
 *Main pri spusten� platformy zavol� funkciu na inicializ�ciu platformy a po ukon�en�
 *inicializ�cie spust� syst�m do runtime. Po ukon�en� runtime uskusto�n� "clean-up" syst�mu.
 */

/****************************** Include Files ********************************/

#include "Platform.h"
#include "Scheduler.h"


/**************************** Macro Definitions ********************************/




/*************************** Function Prototypes *******************************/




/***************************** Global Variables ******************************/




/*************************** Function Definitions ****************************/

 /****************************************************************************/
 /**
 *
 * \fn void main(void)
 *
 *Funkcia main pri spusten� platformy zavol� funkciu na inicializ�ciu platformy a po ukon�en�
 *inicializ�cie spust� syst�m do runtime. Po ukon�en� runtime uskuto�n� "clean-up" syst�mu.
 *
 *
 * @return	void
 *
 *
 ****************************************************************************/
int main(void) {

	Platform_init();

	while(1){

		Scheduler_run();
	}

	Platform_cleanup();
}


