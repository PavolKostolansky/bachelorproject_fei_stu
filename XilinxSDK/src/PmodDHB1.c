/**
 * @file PmodDHB1.c
 * @brief PmodDHB1 sl��i ako roz��ren� rozhranie PWM ovl�da�a.
 *
 *## Funkcionalita PmodDHB1
 *Hlavn� �loha je pr�ca je zjednodu�enie pr�ce s ovl�da�om PWM, ktor� je vytvoren� na
 *interakciu s perif�riou PmodDHB1.
 */

/****************************** Include Files ********************************/
#include "PlatformConfig.h"
#include "PmodDHB1.h"
#include "PWM.h"
#include "xil_io.h"



/**************************** Macro Definitions ********************************/

#define DHB1_GPIO_CHANNEL_OFFSET 0x8


/*************************** Function Prototypes *******************************/

static void DHB1_motorEnable();

static void DHB1_motorDisable();

/***************************** Global Variables ******************************/

PmodDHB1 pmodDHB1;


/************************** Function Definitions ***************************/


/****************************************************************************/
/**
*
* \fn void PmodDHB1_init()
*
* Funkcia sl��i na inicializ�ciu �trukt�ry PmodDHB1 spr�vnymi parametrami perif�rie PmodDHB1.
* Je volan� pri inicializ�ci� platformy.
*
*
* @return	void
*
*
****************************************************************************/
void PmodDHB1_init(){

	pmodDHB1.GPIO_addr = GPIO_BASEADDR;
	pmodDHB1.PWM_addr  = PWM_BASEADDR;
	pmodDHB1.PWM_per   = CLK_FREQ * PWM_PER / 1000;

	PWM_Set_Period(PWM_BASEADDR, pmodDHB1.PWM_per);

	DHB1_motorEnable();

}

/****************************************************************************/
/**
*
* \fn void PmodDHB1_motorSetVoltage(u8 ID,u8 *data)
*
* Funkcia je volan� z aplika�nej vrstvy a sl��i na prepo��tanie voltov (ak�n� z�sah)
* na PWM. N�sledne je nastaven� strieda PWM pomocou funkcie PWM_Set_Duty, ktor� pon�ka
* ovl�da� PWM.
*
* @param
*    [in] ID:  ID volania funkcie.
* @param
*    [in] data: pointer na typ u8.
*
* @return	void
*
*
****************************************************************************/
void PmodDHB1_motorSetVoltage(u8 ID,u8 *data){


	float* voltage;

	voltage = (float*)data ;

	u32 dutyCycle = 0,
		PWM_addr = pmodDHB1.PWM_addr,
		PWM_per  = pmodDHB1.PWM_per;


	dutyCycle = (PWM_per*(*voltage)) / MAX_VOLTAGE;


	PWM_Set_Duty(PWM_addr, dutyCycle, 0);


}


/****************************************************************************/
/**
*
* \fn static void DHB1_motorEnable()
*
* Funkcia sl��i na povolenie generovania PWM sign�lu.
*
*
* @return	void
*
*
****************************************************************************/
static void DHB1_motorEnable() {
	PWM_Enable(pmodDHB1.PWM_addr);
}


/****************************************************************************/
/**
*
* \fn static void DHB1_motorDisable()
*
* Funkcia sl��i na zak�zanie generovania PWM sign�lu.
*
*
* @return	void
*
*
****************************************************************************/
static void DHB1_motorDisable() {
	PWM_Disable(pmodDHB1.PWM_addr);
}


/****************************************************************************/
/**
*
* \fn void DHB1_setDir(u8 ID,u8 *data)
*
* Funkcia sl��i na nastavenie smeru ot��ania motora.
*
* @param
*    [in] dir:  Smer ot��ania motora.
*
*
* @return	void
*
*
****************************************************************************/
void DHB1_setDir(u8 ID,u8 *data) {

	DHB1_motorDisable();
	Xil_Out8(pmodDHB1.GPIO_addr, data[0]);
	DHB1_motorEnable();

}

