/**
 * @file MotorFeedback.c
 * @brief MotorFeedback sl��i ako ovl�da� pre perif�riu PmodDHB1.
 *
 *## Funkcionalita MotorFeedback
 *Hlavn� �loha ovl�da�a MotorFeedback je  aktualiz�cia po�tu n�be�n�ch hr�n z enk�dera,
 *ktor� s� ulo�ene v registroch perif�rie PmodDHB1 a posiela� aktu�lne inform�cie do aplika�nej vrstvy.
 */

/****************************** Include Files ********************************/
#include "COMM.h"
#include "MotorFeedback.h"
#include "PlatformConfig.h"


/**************************** Macro Definitions ********************************/

#define MOTORFEEDBACK_CLEAR_OFFSET    0x00
#define MOTORFEEDBACK_M1_POS_OFFSET   0x04


/*************************** Function Prototypes *******************************/

static void MotorFeedback_clearEdgeCounter();


/***************************** Global Variables ******************************/

MotorFeedback motorFeedback;

/*************************** Function Definitions ****************************/

/****************************************************************************/
/**
*
* \fn void MotorFeedback_init()
*
* Funkcia sl��i na inicializ�ciu �trukt�ry MotorFeedback spr�vnymi parametrami perif�rie
* PmodDHB1 - MotorFeedback. Je volan� pri inicializ�ci� platformy.
*
*
* @return	void
*
*
****************************************************************************/
void MotorFeedback_init(){

	 motorFeedback.baseAddr = MOTOR_FB_BASEADDR;
	 motorFeedback.clkFreqHz = CLK_FREQ;
	 motorFeedback.edgesPerRev = SENSOR_EDGES_PER_REV;
	 motorFeedback.gearboxRatio = GEARBOX_RATIO;
}


/****************************************************************************/
/**
*
* \fn void MotorFeedback_updateEdgesCount()
*
* Funkcia sl��i na aktualiz�ciu po�tu n�be�n�ch hr�n z enk�dera, ktor� s� najsk�r
* pre��tan� zo suma�n�ch registrov, potom sa d�ta odo�l� do aplika�nej vrstvy a
* suma�n� registre sa zresetuj�. Funkcia je volan� s peri�dou 10ms.
*
*
* @return	void
*
*
****************************************************************************/
void MotorFeedback_updateEdgesCount(){

	u16 edgesCount = 0;
	u8 edgesCountArray[2] = {0};
	u32 m1Pos_addr = motorFeedback.baseAddr + MOTORFEEDBACK_M1_POS_OFFSET;

	edgesCount = (u16) (Xil_In32(m1Pos_addr) >> 16);

	edgesCountArray[0] = (u8) edgesCount;
	edgesCountArray[1] = (u8) (edgesCount >> 8);

	COMM_receive( MOTOR_FEEDBACK_EDGES_COUNT, edgesCountArray );

	MotorFeedback_clearEdgeCounter();
}


/****************************************************************************/
/**
*
* \fn static void MotorFeedback_clearEdgeCounter()
*
* Funkcia sl��i na resetovanie hodn�t v suma�n�ch registroch perif�rie PmodDHB1.
*
*
* @return	void
*
*
****************************************************************************/
static void MotorFeedback_clearEdgeCounter() {

	   Xil_Out8(motorFeedback.baseAddr + MOTORFEEDBACK_CLEAR_OFFSET, 0x1);
	   Xil_Out8(motorFeedback.baseAddr + MOTORFEEDBACK_CLEAR_OFFSET, 0x0);
	   Xil_Out8(motorFeedback.baseAddr + MOTORFEEDBACK_CLEAR_OFFSET, 0x2);
	   Xil_Out8(motorFeedback.baseAddr + MOTORFEEDBACK_CLEAR_OFFSET, 0x0);

}
