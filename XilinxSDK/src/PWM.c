/**
 * @file PWM.c
 * @brief PWM sl��i na interakciu s perif�riou PmodDHB1.
 *
 *## Funkcionalita PWM
 *Hlavn� �loha je pr�ca s perif�riou PmodDHB1, a to presnej�ie zmena PWM sign�lu, zmena
 *smeru ot��ania motora a povolenie alebo zak�zanie generovania PWM sign�lu.
 */

/***************************** Include Files *******************************/
#include "PWM.h"
#include "xil_io.h"

/************************** Function Definitions ***************************/


/****************************************************************************/
/**
*
* \fn void PWM_Set_Period(u32 baseAddr, u32 clocks)
*
* Funkcia sl��i na nastavenie striedy generovan�ho PWM sign�lu.
*
* @param
*    [in] baseAddr:  Adresa perif�rie PmodDHB1, presnej�ie adresa PWM.
*
* @param
*    [in] clocks:  Prepo��tan� hodnota peri�dy PWM sign�lu.
*
* @return	void
*
*
****************************************************************************/
void PWM_Set_Period(u32 baseAddr, u32 clocks)
{
	Xil_Out32(baseAddr + PWM_AXI_PERIOD_REG_OFFSET, clocks);
}



/****************************************************************************/
/**
*
* \fn void PWM_Set_Duty(u32 baseAddr, u32 clocks, u32 pwmIndex)
*
* Funkcia sl��i na nastavenie striedy generovan�ho PWM sign�lu.
*
* @param
*    [in] baseAddr:  Adresa perif�rie PmodDHB1, presnej�ie adresa PWM.
*
* @param
*    [in] clocks:  Prepo��tan� hodnota striedy pre porovn�vac� register.
*
** @param
*    [in] pwmIndex:  Premenn� na zvolenie kan�lu PWM.
*
* @return	void
*
*
****************************************************************************/
void PWM_Set_Duty(u32 baseAddr, u32 clocks, u32 pwmIndex)
{
	Xil_Out32(baseAddr + PWM_AXI_DUTY_REG_OFFSET + (4*pwmIndex), clocks);
}



/****************************************************************************/
/**
*
* \fn u32 PWM_Get_Period(u32 baseAddr)
*
* Funkcia sl��i na z�skanie aktu�lnej peri�dy PWM sign�lu.
*
* @param
*    [in] baseAddr:  Adresa perif�rie PmodDHB1, presnej�ie adresa PWM.
*
*
* @return	u32
*
*
****************************************************************************/
u32 PWM_Get_Period(u32 baseAddr)
{
	return Xil_In32(baseAddr + PWM_AXI_PERIOD_REG_OFFSET);
}



/****************************************************************************/
/**
*
* \fn u32 PWM_Get_Duty(u32 baseAddr, u32 pwmIndex)
*
* Funkcia sl��i na z�skanie aktu�lnej striedy sign�lu PWM.
*
* @param
*    [in] baseAddr:  Adresa perif�rie PmodDHB1, presnej�ie adresa PWM.
*
* @param
*    [in] pwmIndex:  Premenn� na zvolenie kan�lu PWM.
*
*
* @return	u32
*
*
****************************************************************************/
u32 PWM_Get_Duty(u32 baseAddr, u32 pwmIndex)
{
	return Xil_In32(baseAddr + PWM_AXI_DUTY_REG_OFFSET + (4*pwmIndex));
}



/****************************************************************************/
/**
*
* \fn void PWM_Enable(u32 baseAddr)
*
* Funkcia sl��i na povolenie generovania PWM sign�lu.
*
* @param
*    [in] baseAddr:  Adresa perif�rie PmodDHB1, presnej�ie adresa PWM.
*
*
* @return	void
*
*
****************************************************************************/
void PWM_Enable(u32 baseAddr)
{
	Xil_Out32(baseAddr + PWM_AXI_CTRL_REG_OFFSET, 1);
}



/****************************************************************************/
/**
*
* \fn void PWM_Disable(u32 baseAddr)
*
* Funkcia sl��i na zak�zanie generovania PWM sign�lu.
*
* @param
*    [in] baseAddr:  Adresa perif�rie PmodDHB1, presnej�ie adresa PWM.
*
*
* @return	void
*
*
****************************************************************************/
void PWM_Disable(u32 baseAddr)
{
	Xil_Out32(baseAddr + PWM_AXI_CTRL_REG_OFFSET, 0);
}
