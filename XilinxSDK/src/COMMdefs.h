/**
 * @file COMMdefs.h
 * @brief S�bor obsahuje makr� pou�ite v "route tables"
 *
 */

#ifndef SRC_COMMDEFS_H_
#define SRC_COMMDEFS_H_

/****************************** Include Files ********************************/




/**************************** Macro Definitions ********************************/

/* RX table defs func*/

#define APP_RECEIVE 0


/* RX table defs IDs */
#define UART_RECEIVED_DATA 0
#define MOTOR_FEEDBACK_EDGES_COUNT 1
#define MOTOR_DECODER_TIME_DIFF 2
#define MOTOR_DECODER_DIR 3
#define MOTOR_DECODER_EDGES_COUNT 4



/* TX table defs func*/
#define UART_WRITE 0
#define PMOD_DHB1_SET_VOLTAGE 1
#define PMOD_DHB1_SET_DIR 2

/* TX table defs IDs */
#define UART_WRITE_NUMBER 0
#define UART_WRITE_CHAR 1
#define PMOD_DHB1_SET_VOLTAGE_M0 2
#define PMOD_DHB1_SET_DIR_M0 3



/*************************** Function Prototypes *******************************/




/***************************** Global Variables ******************************/





#endif /* SRC_COMMDEFS_H_ */
