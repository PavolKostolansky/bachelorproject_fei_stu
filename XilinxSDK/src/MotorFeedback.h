/**
 * @file MotorFeedback.h
 * @brief S�bor obsahuje API, ktor� pon�ka MotorFeedback ovl�da� a taktie� �trukt�ru MotorFeedback.
 *
 */

#ifndef MOTORFEEDBACK_H
#define MOTORFEEDBACK_H

/****************************** Include Files ********************************/
#include "xil_io.h"
#include "xil_types.h"
#include "xstatus.h"


/**************************** Macro Definitions ********************************/




/**************************** Type Definitions *****************************/

/** @struct MotorFeedback
 *  @brief T�to �trukt�ra sl��i na uchovanie parametrov perif�rie PmodDHB1 - MotorFeedback
 *  @var MotorFeedback::baseAddr
 *  Premenn� 'baseaddr' uchov�va adresu perif�rie pre AXI protokol
 *  @var MotorFeedback::clkFreqHz
 *  Premenn� 'clkFreqHz' uchov�va frekvenciu, na ktor� je pripojen� perif�ria PmodDHB1 - MotorFeedback
 *  @var MotorFeedback::edgesPerRev
 *  Premenn� 'edgesPerRev' uchov�va po�et impulzov, ktor� je vygenerovan� enk�derom za jednu ot��ku
 *  @var MotorFeedback::gearboxRatio
 *  Premenn� 'gearboxRatio' uchov�va prevodov� pomer motora
 */
typedef struct MotorFeedback {
   u32 baseAddr;
   u32 clkFreqHz;
   u32 edgesPerRev;
   u32 gearboxRatio;
} MotorFeedback;

/*************************** Function Prototypes *******************************/

void MotorFeedback_init();

void MotorFeedback_updateEdgesCount();




#endif // MOTORFEEDBACK_H
