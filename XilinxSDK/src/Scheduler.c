/**
 * @file Scheduler.c
 * @brief Scheduler sl��i na �asovanie funkci�.
 *
 *## Funkcionalita Scheduler
 *Hlavn� �loha Schedulera je deterministick� �asovanie behu funkci� v platforme.
 *Peri�du pre beh konkr�tnej funkcie je mo�n� nastavi� v "SchedulerTable".
 */

/***************************** Include Files *******************************/
#include "Scheduler.h"
#include "AppHandle.h"
#include "MotorFeedback.h"
#include "MotorDecoder.h"
#include "PlatformConfig.h"

/*************************** Function Prototypes *******************************/

/****************************************************************************/
/**
*
* \fn static void Scheduler_emptyFunc()
*
* Funkcia sl��i na vyplnenie po�a smern�kov, v miestach, kedy nebe�ia �iadne funkcie.
*
*
* @return	void
*
*
****************************************************************************/
static void Scheduler_emptyFunc(){};

/****************************************************************************/
/** @brief Glob�lna statick� premenn� typu po�a void smern�kov, pre �asovanie funkci�.
*
* Prvky s� umiest�ovan� tak aby sme dosiahli po�adovan� peri�du volania.
****************************************************************************/
static void (*SchedulerTable[10])() = {

	/*0*/   MotorDecoder_updateMeasuredValues,
	/*1*/   AppHandle_sendUART,
	/*2*/   Scheduler_emptyFunc,
	/*3*/   Scheduler_emptyFunc,
	/*4*/   Scheduler_emptyFunc,
	/*5*/   Scheduler_emptyFunc,
	/*6*/   Scheduler_emptyFunc,
	/*7*/   Scheduler_emptyFunc,
	/*8*/   Scheduler_emptyFunc,
	/*9*/   Scheduler_emptyFunc,

};

/***************************** Global Variables ******************************/

volatile u8 tick=0;

/*************************** Function Definitions ****************************/

/****************************************************************************/
/**
*
* \fn void Scheduler_run()
*
* Funkcia sl��i na �asovanie jednotliv�ch funkci�, ktor� musia be�a� deterministicky
* a s presnou peri�dou. Je volan� z funkcie main, a premenn� tick je inkrementovan�
* s peri�dou 1 ms pomocou preru�enia z �asova�a.
*
*
* @return	void
*
*
****************************************************************************/
void Scheduler_run(){

	static u8 id=0;
	if(tick){

		if(id > 9)id=0;
		SchedulerTable[id]();

		if(tick > TIMER_PERIOD)
		{
			id++;
		}
		id++;
		tick=0;

	}
}


/****************************************************************************/
/**
*
* \fn void Scheduler_timerCounterHandler(void *CallBackRef, u8 TmrCtrNumber)
*
* Funkcia sl��i na inkrementovanie premennej tick s peri�dou 1 ms.
*
* @param
*    [in] CallBackRef:  Pointer na typ void.
*
* @param
*    [in] TmrCtrNumber:  Premenn� typu u8, charakterizuj�ca pou�it� �asova�, v pr�pade,
*    �e ich je v syst�me viac.
*
*
* @return	void
*
*
****************************************************************************/
void Scheduler_timerCounterHandler(void *CallBackRef, u8 TmrCtrNumber){


	tick++;

}


