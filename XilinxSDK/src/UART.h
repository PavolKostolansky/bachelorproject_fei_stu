/**
 * @file UART.h
 * @brief S�bor obsahuje API, ktor� pon�ka UART ovl�da�.
 *
 */

#ifndef SRC_UART_H_
#define SRC_UART_H_

/***************************** Include Files *******************************/
#include "xuartlite.h"

/************************** Function Prototypes ******************************/
void UART_init();

void UART_write(u8 ID, u8 *data);

/***************************** Global Variables ******************************/

XUartLite UartLite;

#endif /* SRC_UART_H_ */
