/**
 * @file COMM.c
 * @brief Komunika�n� vrstva, ktor� sp�ja aplika�n� vrstvu s ni���mi vrstvami platformy.
 *
 *## Funkcionalita vrstvy COMM
 *t�to vrstva vytv�ra prepojenia medzi ovl�da�mi a aplika�nou vrstvou cez "route tables", ktor� obsahuj�
 *adresy jednotliv�ch funkci�.
 */


/****************************** Include Files ********************************/

#include "COMM.h"
#include "AppHandle.h"
#include "UART.h"
#include "PmodDHB1.h"

/**************************** Macro Definitions ********************************/



/*************************** Function Prototypes *******************************/


static void (*COMM_RX_routeTable[1])(u8 ID,u8 *data) = {
		/*0*/   AppHandle_receive, // APP_RECEIVE

};


static void (*COMM_TX_routeTable[3])(u8 ID,u8 *data) = {
	/*0*/  UART_write,						// UART_WRITE_NUMBER
	/*1*/  PmodDHB1_motorSetVoltage,		// PMOD_DHB1_SET_VOLTAGE
	/*2*/  DHB1_setDir,						// PMOD_DHB1_SET_DIR
};


/***************************** Global Variables ******************************/



static u8 COMM_RX_IDMap[5] = {
	/*0*/ APP_RECEIVE,  // UART_RECEIVED_DATA
	/*1*/ APP_RECEIVE,  // MOTOR_FEEDBACK_EDGES_COUNT
	/*2*/ APP_RECEIVE,  // MOTOR_DECODER_TIME_DIFF
	/*3*/ APP_RECEIVE,  // MOTOR_DECODER_DIR
	/*4*/ APP_RECEIVE,  // MOTOR_DECODER_EDGES_COUNT

};


static u8 COMM_TX_IDMap[4] = {
	/*0*/ UART_WRITE,	 			// UART_WRITE_NUMBER
	/*1*/ UART_WRITE,    			// UART_WRITE_CHAR
	/*2*/ PMOD_DHB1_SET_VOLTAGE,    // PMOD_DHB1_SET_VOLTAGE_M0
	/*3*/ PMOD_DHB1_SET_DIR,    	// PMOD_DHB1_SET_VOLTAGE_M0

};


/*************************** Function Definitions ****************************/

/****************************************************************************/
/**
*
* \fn void COMM_Receive(u8 ID,u8 *data)
*
* Funkcia, ktor� je volan� zo spodn�ch vrstiev (ovl�da�ov) platformy a pres�vaj� sa pomocou nej d�ta
* smerom do aplika�nej vrstvy.
*
* @param
*    [in] ID:  ID API, z ktorej prich�dza volanie funkcie.
* @param
*    [in] data: pointer na typ u8.
*
* @return	void
*
*
****************************************************************************/
void COMM_Receive(u8 ID,u8 *data){

	COMM_RX_routeTable[COMM_RX_IDMap[ID]](ID,data);

}


/****************************************************************************/
/**
*
* \fn void COMM_Transmit(u8 ID,u8 *data)
*
* Funkcia, ktor� je volan� v aplika�nej vrstve platformy a pres�vaj� sa pomocou nej d�ta
* smerom do spodn�ch vrstiev platformy (ovl�da�ov).
*
* @param
*    [in] ID:  ID API, z ktorej prich�dza volanie funkcie.
* @param
*    [in] data: pointer na typ u8.
*
* @return	void
*
*
****************************************************************************/
void COMM_Transmit(u8 ID,u8 *data){

	COMM_TX_routeTable[COMM_TX_IDMap[ID]](ID,data);

}
