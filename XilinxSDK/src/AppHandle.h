/**
 * @file AppHandle.h
 * @brief S�bor obsahuje API, ktor� pon�ka AppHandle.
 *
 */

#ifndef SRC_APPHANDLE_H_
#define SRC_APPHANDLE_H_

/****************************** Include Files ********************************/

#include "Types.h"


/**************************** Macro Definitions ********************************/




/*************************** Function Prototypes *******************************/

void AppHandle_receive(u8 ID,u8 *data);

void AppHandle_sendUART();






/***************************** Global Variables ******************************/






#endif /* SRC_APPHANDLE_H_ */
