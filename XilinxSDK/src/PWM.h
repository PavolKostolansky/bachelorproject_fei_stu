/**
 * @file PWM.h
 * @brief S�bor obsahuje API, ktor� pon�ka PWM ovl�da� a taktie� makr� s "offsetmi".
 *
 */


#ifndef PWM_H
#define PWM_H


/****************************** Include Files ********************************/
#include "xil_types.h"
#include "xstatus.h"

/**************************** Macro Definitions ********************************/
#define PWM_AXI_CTRL_REG_OFFSET 0
#define PWM_AXI_PERIOD_REG_OFFSET 8
#define PWM_AXI_DUTY_REG_OFFSET 64

/*************************** Function Prototypes *******************************/

void PWM_Set_Period(u32 baseAddr, u32 clocks);

void PWM_Set_Duty(u32 baseAddr, u32 clocks, u32 pwmIndex);

u32 PWM_Get_Period(u32 baseAddr);

u32 PWM_Get_Duty(u32 baseAddr, u32 pwmIndex);

void PWM_Enable(u32 baseAddr);

void PWM_Disable(u32 baseAddr);

#endif // PWM_H
